#!/bin/bash
docker build -t nettematic/apache-php:5.5 ./apache-php5.5
docker build -t nettematic/apache-php:5.6 ./apache-php5.6
docker build -t nettematic/apache-php:7.0 ./apache-php7.0
docker build -t nettematic/apache-php:7.1 ./apache-php7.1
docker build -t nettematic/apache-php:7.2 ./apache-php7.2
